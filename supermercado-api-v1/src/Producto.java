
public class Producto implements Comparable<Producto>{
	
	protected int idTipoProducto;
	protected String nombre;
	protected double litros;
	protected int precio;
	protected int contenido;
	protected String unidadVenta;
	
	public Producto () {
		
	}

	public Producto(String nombre, double litros, int precio) {
		this.nombre = nombre;
		this.litros = litros;
		this.precio = precio;
		idTipoProducto = 1;
	}

	public Producto(String nombre, int contenido, int precio) {
		this.nombre = nombre;
		this.contenido = contenido;
		this.precio = precio;
		idTipoProducto = 2;
	}

	public Producto(String nombre, int precio, String unidadVenta) {
		this.nombre = nombre;
		this.precio = precio;
		this.unidadVenta = unidadVenta;
		idTipoProducto = 3;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getLitros() {
		return litros;
	}

	public void setLitros(double litros) {
		this.litros = litros;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

	public int getContenido() {
		return contenido;
	}

	public void setContenido(int contenido) {
		this.contenido = contenido;
	}

	public String getUnidadVenta() {
		return unidadVenta;
	}

	public void setUnidadVenta(String unidadVenta) {
		this.unidadVenta = unidadVenta;
	}

	public String toString() {
		 
		if(idTipoProducto == 1) {
			return "- Nombre: " + nombre + " /// " +"Litros: " + litros + " /// "  
						+  "Precio: $" + precio;
		}else if(idTipoProducto == 2) {
			return "- Nombre: " + nombre + " /// " +"Contenido: " + contenido + " /// "  
						+  "Precio: $" + precio;
		}else {
			return "- Nombre: " + nombre + " /// " +"Precio: $" + precio + " /// "  
						+  "Unidad de venta: " + unidadVenta;
		}
	}

	@Override
	public int compareTo(Producto o) {
		return this.precio - o.precio;
	}

	
}
