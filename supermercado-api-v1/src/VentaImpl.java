import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class VentaImpl implements Venta{

	@Override
	public List<Producto> setProductos() {
		
		List<Producto> listaProductos = new ArrayList();
		
		listaProductos.add(new Producto("Huevo",12,27));
		listaProductos.add(new Producto("Papa",24,"kg"));
		listaProductos.add(new Producto("Agua",5.5,22));
		listaProductos.add(new Producto("Leche",1.5,29));
		
		return listaProductos;
	}

	@Override
	public List<Producto> orderProductos() {
	
		List<Producto> listProductos = setProductos();
		
		Collections.sort(listProductos, new ComparatorProducto());
		
		return listProductos;
	}

	@Override
	public void printProductos(List<Producto> listProductos) {
	
		List<Producto> newListaProductos = listProductos;
		
		newListaProductos.forEach(lp -> {System.out.println(lp);});
	}

	@Override
	public void getProductosDePrecioMenor() {
		
		List<Producto> listProductos = setProductos();
		
		Collections.sort(listProductos);
		
		listProductos.stream().filter(lp -> lp.precio < 25)
		.forEach(System.out::println);
		
	}

	@Override
	public void getProductoMasCaroYBarato() {
		
		List<Producto> listProductos = setProductos();
		
		Collections.sort(listProductos);
		
		System.out.println("Producto m�s barato: " + listProductos.get(0).getNombre());
		System.out.println("Producto m�s caro: " + listProductos.get(listProductos.size()-1).getNombre());
		
	}

}
