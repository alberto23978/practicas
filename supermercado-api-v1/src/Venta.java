import java.util.List;

public interface Venta {
	
	public List<Producto> setProductos();
	
	public List<Producto> orderProductos();
	
	public void printProductos(List<Producto> listProductos);
	
	public void getProductosDePrecioMenor();
	
	public void getProductoMasCaroYBarato();
	
	

}
