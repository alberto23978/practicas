package comprobante;
import ventas.Venta;
import ventas.VentaImpl;

public class Ticket {

	public static void main(String[] args) {
		
		Venta venta = new VentaImpl();
		
		System.out.println("================================");
		System.out.println("Todos los Productos:");
		System.out.println("================================");
		venta.printProductos(venta.orderProductos());
		System.out.println("================================");
		System.out.println("Productos de precio menor a $25");
		System.out.println("================================");
		venta.getProductosDePrecioMenor();
		System.out.println("================================");
		venta.getProductoMasCaroYBarato();
		
		

	}

}
